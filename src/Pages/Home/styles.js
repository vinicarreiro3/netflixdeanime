import styled from "styled-components";

export const NetflixWallpaper = styled.section`
  height: 100vh;
  width: 100vw;
`;
export const TopContainer = styled.div`
  display: flex;
`;
export const HomeText = styled.div`
  display: flex;
  align-items: center;
  margin-top: 250px;
  margin-left: 37.5vw;
  h1 {
    font-size: 40px;
  }
  h3 {
    font-size: 20px;
  }

  flex-direction: column;
  width: 25vw;
  margin-right: 0px;
  button {
    width: 20vw;
  }
`;
