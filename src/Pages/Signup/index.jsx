import HomeHeader from "../../components/HomeHeader";
import RegisterBar from "../../components/RegisterBar";
import { UrlNetflixWpp } from "../../utils/url";
import { NetflixWallpaper } from "../Home/styles";
import { RegisterPage } from "./styles";

const Signup = () => {
  return (
    <RegisterPage>
      <HomeHeader login={true}></HomeHeader>
      <RegisterBar></RegisterBar>
    </RegisterPage>
  );
};

export default Signup;
