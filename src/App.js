import PathRoutes from "./Routes";
import "./App.css";

function App() {
  return (
    <div className="page">
      <PathRoutes />
    </div>
  );
}

export default App;
