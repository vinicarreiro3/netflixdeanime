import styled from "styled-components";

export const RegisterContainer = styled.div`
  border-top: 1px grey solid;
`;
export const RegisterForm = styled.form`
  p {
    color: black;
  }
`;
export const RegisterDiv = styled.div`
  width: 100vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 30vw;
  margin: 0 35vw;
  h1 {
    color: grey;
    font-size: 40px;
  }
`;
