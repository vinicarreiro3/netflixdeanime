import styled from "styled-components";

export const LoginMenu = styled.div`
  width: 382px;
  height: 450px;
  background-color: rgba(0, 0, 0, 0.9);
  display: flex;
  flex-direction: column;
  padding: 0 50px;
`;

export const LoginForm = styled.form`
  display: flex;
  flex-direction: column;
`;
